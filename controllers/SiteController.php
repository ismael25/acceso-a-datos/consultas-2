<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Maillot;
use app\models\Equipo;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionConsultaorm(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("COUNT(dorsal) AS Ciclistas"),
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista "
        ]);
    }
    public function actionConsultadao(){
        

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(dorsal) AS Ciclistas FROM ciclista ',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista"
        ]);
    }
    public function actionConsultaorm2(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("COUNT(dorsal) AS ciclistas_banesto")
                ->where("nomequipo='Banesto'")
                ->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['ciclistas_banesto'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>" SELECT COUNT(*) FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    public function actionConsultadao2(){
        
    
        $dataProvider = new SqlDataProvider([
            'sql'=> " SELECT COUNT(dorsal) AS ciclistas_banesto FROM ciclista WHERE nomequipo = 'Banesto'",
            'pagination' =>[
                 'pageSize' => 5,
            ]
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['ciclistas_banesto'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>" SELECT COUNT(*) FROM ciclista WHERE nomequipo = 'Banesto'"
        ]);
    }
    public function actionConsultaorm3(){
        
        $dataProvider = new ActiveDataProvider ([
            'query' => Ciclista::find()
                ->select('AVG(edad) As Edad_Media'),
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
         return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['Edad_Media'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>" SELECT AVG(edad) FROM ciclista",
        ]);
    }
    public function actionConsultadao3(){
        
        
        $dataProvider = new SqlDataProvider([
            'sql'=> "SELECT AVG(edad)AS Edad_Media FROM ciclista",
            'pagination' =>[
                 'pageSize' => 5,
            ]
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Edad_Media'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) FROM ciclista"
        ]);
    }public function actionConsultaorm4(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("AVG(edad) AS Edad_Media")
                ->where("nomequipo = 'Banesto'"),
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Edad_Media'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>" SELECT AVG(edad) FROM ciclista WHERE nomequipo = 'Banesto'"
        ]);
    }
    public function actionConsultadao4(){
;
               
        $dataProvider = new SqlDataProvider([
            'sql'=>" SELECT AVG(edad) AS Edad_Media FROM ciclista WHERE nomequipo = 'Banesto'",
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Edad_Media'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>" SELECT AVG(edad) FROM ciclista WHERE nomequipo = 'Banesto'"
        ]);
    }
    public function actionConsultaorm5(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo ,AVG(edad) AS Edad_Media")
                ->GROUPBY('nomequipo'),
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','Edad_Media'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo ,AVG(edad) FROM ciclista GROUP BY nomequipo "
        ]);
    }
    public function actionConsultadao5(){
        
               
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nomequipo ,AVG(edad) AS Edad_Media FROM ciclista GROUP BY nomequipo ",
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','Edad_Media'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo ,AVG(edad) FROM ciclista GROUP BY nomequipo "
        ]);
    }
    public function actionConsultaorm6(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo ,count(nombre) AS Ciclistas ")
                ->GROUPBY ('nomequipo') ,
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo' , 'Ciclistas'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo ,count(nombre) FROM ciclista GROUP BY nomequipo"
        ]);
    }
    public function actionConsultadao6(){
        
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo ,count(nombre) AS Ciclistas FROM ciclista GROUP BY nomequipo',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo' , 'Ciclistas'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo ,count(nombre) FROM ciclista GROUP BY nomequipo"
        ]);
    }
    public function actionConsultaorm7(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("COUNT(nompuerto) AS Puertos"),
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Puertos'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) FROM puerto"
        ]);
    }
    public function actionConsultadao7(){
        

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS Puertos FROM puerto',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Puertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) FROM puerto"
        ]);
    }
    public function actionConsultaorm8(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("COUNT(nompuerto) AS Puertos")
                ->where("altura>1500"),
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Puertos'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) FROM puerto WHERE altura>1500 "
        ]);
    }
    public function actionConsultadao8(){
        
               
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT COUNT(*)AS Puertos FROM puerto WHERE altura>1500 ",
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Puertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) FROM puerto WHERE altura>1500 "
        ]);
    }
    public function actionConsultaorm9(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo ,COUNT(*) AS 'Numero_ciclista'")
                ->GROUPBY("nomequipo")
                ->HAVING('COUNT(*)>4'),
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','Numero_ciclista'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo ,COUNT(*) AS 'Numero_ciclista' FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4"
        ]);
    }
    public function actionConsultadao9(){
        
               
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nomequipo ,COUNT(*) AS 'Numero_ciclista' FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','Numero_ciclista'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo ,COUNT(*) AS 'Numero_ciclista' FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4"
        ]);
    }
    public function actionConsultaorm10(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo,COUNT(*)AS 'Numero_ciclista'")
                ->where("edad BETWEEN 28 AND 32")
                ->GROUPBY ('nomequipo')
                ->HAVING ("COUNT(*)>4"),
            
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','Numero_ciclista'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>" SELECT nomequipo,COUNT(*)AS 'Numero_ciclista' FROM ciclista WHERE edad BETWEEN 28 AND 32  GROUP BY nomequipo HAVING COUNT(*)>4"
        ]);
    }
    public function actionConsultadao10(){
        
  
               
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nomequipo,COUNT(*)AS 'Numero_ciclista' FROM ciclista WHERE edad BETWEEN 28 AND 32  GROUP BY nomequipo HAVING COUNT(*)>4",
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','Numero_ciclista'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo,COUNT(*)AS 'Numero_ciclista' FROM ciclista WHERE edad BETWEEN 28 AND 32  GROUP BY nomequipo HAVING COUNT(*)>4"
        ]);
    }
    public function actionConsultaorm11(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("dorsal, COUNT(*) AS 'Número_etapas'")
                ->GROUPBY('dorsal'),
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','Número_etapas'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) AS 'Número_etapas' FROM etapa GROUP BY dorsal"
        ]);
    }
    public function actionConsultadao11(){
        
               
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT dorsal, COUNT(*) AS 'Número_etapas' FROM etapa GROUP BY dorsal",
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','Número_etapas'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) AS 'Número_etapas' FROM etapa GROUP BY dorsal"
        ]);
    }
    public function actionConsultaorm12(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("dorsal, COUNT(*) AS 'Número_etapas'")
                ->GROUPBY('dorsal')
                ->HAVING ('COUNT(*)>1'),
            'pagination'=> [
                'pageSize'=>5,
            ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','Número_etapas'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal, COUNT(*) AS 'Número_etapas' FROM etapa GROUP BY dorsal HAVING COUNT(*)>1"
        ]);
    }
    public function actionConsultadao12(){
        
               
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT dorsal, COUNT(*) AS 'Número_etapas' FROM etapa GROUP BY dorsal HAVING COUNT(*)>1",
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','Número_etapas'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal, COUNT(*) AS 'Número_etapas' FROM etapa GROUP BY dorsal HAVING COUNT(*)>1"
        ]);
    }
}


